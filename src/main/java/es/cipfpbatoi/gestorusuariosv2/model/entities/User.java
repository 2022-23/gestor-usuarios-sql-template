package es.cipfpbatoi.gestorusuariosv2.model.entities;

import java.time.LocalDate;
import java.util.Objects;


public class User {

    private int id;

    private  String name;

    private String surname;

    private String dni;

    private String email;

    private String zipCode;

    private String mobilePhone;

    private LocalDate birthday;

    private String password;

    public User(int id, String name, String surname, String dni, String email, String zipCode,
                String mobilePhone, LocalDate birthday, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.zipCode = zipCode;
        this.mobilePhone = mobilePhone;
        this.birthday = birthday;
        this.password = password;
        this.dni = dni;
    }

    public User(String name, String surname, String dni, String email, String zipCode,
                String mobilePhone, LocalDate birthday, String password) {
        this(-1, name, surname, dni, email, zipCode, mobilePhone, birthday, password);
    }

    public User(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDni() {
        return dni;
    }

    public String getEmail() {
        return email;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User user = (User) o;
        return getDni().equals(user.getDni());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDni());
    }
}
