package es.cipfpbatoi.gestorusuariosv2.utils.database;

import es.cipfpbatoi.gestorusuariosv2.exceptions.DatabaseConnectionError;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
public class MySQLConnection {

    private Connection connection;

    private String ip;

    private String username;

    private String password;

    private String database;

    public MySQLConnection(){
        this.ip = "127.0.0.1";
        this.database = "crm_db";
        this.username = "root";
        this.password = "123456789";
    }

    public MySQLConnection(String ip, String database, String username, String password) {
        this.ip = ip;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public MySQLConnection(String database, String username, String password) {
        this.ip = "127.0.0.1";
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public Connection getConnection() {
        try {
            if (connection != null && connection.isValid(2)) {
                return connection;
            }
            String dbURL = String.format("jdbc:mysql://%s/%s?serverTimezone=UTC&allowPublicKeyRetrieval=true",
                    ip, database);
            connection = DriverManager.getConnection(dbURL,username,password);
            return connection;
        } catch (SQLException exception) {
            throw new DatabaseConnectionError(exception.getMessage());
        }
    }


}
