package es.cipfpbatoi.gestorusuariosv2.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

public class UserNotFoundException extends HttpServerErrorException {

    public UserNotFoundException(int id) {
        super(HttpStatus.BAD_REQUEST, "El usuario con código" + id + "no ha sido encontrado");
    }

}
