package es.cipfpbatoi.gestorusuariosv2.exceptions;

public class DatabaseConnectionError extends RuntimeException {

    public DatabaseConnectionError(String message) {
        super(message);
    }

}
